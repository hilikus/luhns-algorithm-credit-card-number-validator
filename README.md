
Coding challenge: Credit card number validator using Luhn's algorithm.

The process to validate a credit card number is:

1) put the card number into a reverse order
2) for every other number starting at the second digit, double that number's value. eg: 1 5 2 3 becomes 1 10 2 6
3) add these numbers together, treating 2 digit numbers as two separate values. eg: 1 + 1 + 0 + 2 + 6
4) if the credit card number is valid the total result should be a multiple of 10 eg: 50, 60 , 70 etc