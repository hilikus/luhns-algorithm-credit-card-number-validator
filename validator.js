var cardNumber;
var reversedCardNumber;
var fullNumber = "";
var digit;
var total = 0;

cardNumber = '1 2 3 4 - 5 6 7 8 - 9 0 1 2 - 3 4 5 2';

// remove anything that isn't a digit, turn into an array to reverse the order.
reversedCardNumber = cardNumber.replace(/[^\d]/g,'').split("").reverse();

// cycle through each digit in the array, doubling the every other digit.
for (i = 0; i < reversedCardNumber.length; i++) {
	digit = reversedCardNumber[i];
    if (i % 2 != 0){
    	digit *= 2;
    }
    fullNumber = fullNumber + digit;
}

// add all the individual digits together
for (i = 0; i < fullNumber.length; i++) {
	digit = parseInt(fullNumber.charAt(i), 10);
	total = total + digit;
}

checkValid(total);


// check total is divisible by 10 and not 0
function checkValid(total){
    if (total != 0 && total % 10 == 0){
        alert(true);
    }else{
        alert(false);
    }
}